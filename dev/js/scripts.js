function selectField() {
    $('.select').select2({
        minimumResultsForSearch: -1,
        placeholder: 'Статус'
    });

    //Select2 fix on resize
    $(window).on('resize', function() {
        $('.input_select').each(function() {
            let selectInput = $(this),
                selectInputWidth = selectInput.outerWidth();

            selectInput.find('.select2-container').css('width', selectInputWidth);

        });
    });

    //Select placeholder color fix
    $('.select').on('change', function (evt) {
        let value = $(this).val();
        let placeholder = $(this).closest('.input_select').find('.select2-selection__rendered');
        if(value == 'default'){
            placeholder.css({'color':' #999999'})
        } else{
            placeholder.css({'color':' #111111'})
        }
    })
}

//Функция форматирования цены
function priceFormat(val) {
    return String(val).replace(/(\s)+/g, '').replace(/(\d{1,3})(?=(?:\d{3})+$)/g, '$1 ');
}

//Разрешить ввод только цифр
jQuery.fn.ForceNumericOnly =
    function()
    {
        return this.each( () =>
        {
            $(this).keydown( (e) =>
            {
                let key = e.charCode || e.keyCode || 0;
                return (
                key == 8 ||
                key == 9 ||
                key == 46 ||
                (key >= 37 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
            });
        });
    };


//Форматирование цены при вводе в поле
$('.input__field_price').keyup(function () {
    let value = $(this).val();
    $(this).val(priceFormat(value));
});

$('.input__field_price, .input__field_year').ForceNumericOnly();

//Ограничиваем длину поля с годом
$('.input__field_year').keydown(function () {
   let value = $(this).val();
   if(value > 3){
       $(this).val(value.substring(0, 3));
   }
});

selectField();
