var gulp = require('gulp'), // Подключаем Gulp
    sass = require('gulp-sass'), //Подключаем Sass пакет,
    browserSync = require('browser-sync'), // Подключаем Browser Sync
    concat = require('gulp-concat'), // Подключаем gulp-concat (для конкатенации файлов)
    uglify = require('gulp-uglifyjs'), // Подключаем gulp-uglifyjs (для сжатия JS)
    cssnano = require('gulp-cssnano'), // Подключаем пакет для минификации CSS
    rename = require('gulp-rename'), // Подключаем библиотеку для переименования файлов
    imagemin = require('gulp-imagemin'), // Подключаем библиотеку для работы с изображениями
    pngquant = require('imagemin-pngquant'), // Подключаем библиотеку для работы с png
    cache = require('gulp-cache'), // Подключаем библиотеку кеширования
    autoprefixer = require('gulp-autoprefixer'),// Подключаем библиотеку для автоматического добавления префиксов
    csscomb = require('gulp-csscomb'), // Для удобства чтения css кода, распределения по функциональным частям.
    combineMq = require('gulp-combine-mq'), // Для добавления медиа запросов в код препроцессора CSS.
    htmlbeautify = require('gulp-html-beautify'), // Для форматирования HTML кода
    fileinclude = require('gulp-file-include'), // Для функции шаблонизатора - включение отдельных компонентов в файл.
    sourcemaps = require('gulp-sourcemaps'), // SorceMaps
    mainBowerFiles = require('main-bower-files'), // Перенос библиотек в определенную папку
    concatCss = require('gulp-concat-css'), // Для объединения css файлов (так как concat работает тупо)
    useref = require('gulp-useref'), // Для изменения путей к файлам скриптов и стилям в html
    plumber = require('gulp-plumber'), // Для отладки ошибок Gulp
    gulpif = require('gulp-if'); // Для добавления условий
babel = require('gulp-babel'); // Для преобразования ES6 в javascript

var htmlAssetsPath = 'app/',
    cssAssetsPath = 'app/css/',
    jsAssetsPath = 'app/js/';

var dev = true; // Режим разработки

// По умолчанию при вызове команды Gulp будет вызываться таск watch.
gulp.task('default', ['watch', 'browser-sync', 'scripts', 'html', 'scss', 'img', 'fonts', 'exportScripts', 'bowerExport', 'css-libs']);

gulp.task('watch', function () {
    gulp.watch('dev/scss/**/*.scss', ['scss']);
    gulp.watch('dev/**/*.html', ['html']);
    gulp.watch('dev/fonts/**/*', ['fonts']);
    gulp.watch('dev/js/**/*', ['exportScripts']);
    gulp.watch('dev/img/**/*', ['img']);

    /*gulp.watch('app/!**!/!*.html').on('change', browserSync.reload);
     gulp.watch('app/css/!**!/!*.css').on('change', browserSync.reload);
     gulp.watch('app/js/!**!/!*.js').on('change', browserSync.reload);
     gulp.watch('app/fonts/!**!/!*').on('change', browserSync.reload);*/
});

// Сервер
gulp.task('browser-sync', function () {
    browserSync({ // Выполняем browserSync
        server: { // Определяем параметры сервера
            baseDir: 'app' // Директория для сервера - app
        },
        port: 8080,
        open: true,
        notify: false // Отключаем уведомления
    });
});


// Таск для html компонентов
gulp.task('html', function () {
    gulp.src('dev/*.html')
        .pipe(plumber()) // Делаем отладку модуля
        .pipe(fileinclude({ // Инициализация плагина fileinclude
            prefix: '@@',
            basepath: 'dev/html/'
        }))
        .pipe(useref({
            noAssets: true // Отмена стандартной конкатенации (так как мы сами это сжимаем и переносим)
        }))
        .pipe(htmlbeautify()) // Форматируем html
        .pipe(gulp.dest(htmlAssetsPath)); // Выгружаем в папку app
});


// Таск для Sass
gulp.task('scss', function () {
    return gulp.src('dev/scss/style.+(scss|sass)') // Берем источник
        .pipe(plumber()) // Проверяем на ошибки
        /*.pipe(gulpif(dev, sourcemaps.init()))*/
        .pipe(sass()) // Преобразуем Sass в CSS посредством gulp-sass
        .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], {cascade: true})) // Создаем префиксы
        .pipe(combineMq({
            beautify: dev
        }))
        /*.pipe(gulpif(dev, sourcemaps.write()))*/
        .pipe(concatCss("style.css"))
        .pipe(csscomb()) // Причесываем код CSS
        .pipe(gulp.dest(cssAssetsPath)); // Выгружаем результата в папку app/css
});

// Сжатие и конкатенация скриптов CSS
gulp.task('css-libs', ['scss'], function () {
 return gulp.src([
     'dev/libs/select2.css'
 ])
 .pipe(plumber()) // Проверяем на ошибки
 .pipe(concatCss("libs.min.css")) // Объединяем
 .pipe(cssnano()) // Сжимаем
 .pipe(gulp.dest('app/css')); // Выгружаем в папку app/css
 });


// Сжатие и конкатенация скриптов JS
gulp.task('scripts', function () {
    return gulp.src([ // Берем все необходимые библиотеки
        'dev/libs/jquery.js',
         'dev/libs/select2.js'
    ])
    // ВАЖНО: ЕСЛИ 1 ФАЙЛ ТО ФУНКЦИЯ CONCAT БУДЕТ РАБОТАТЬ НЕВЕРНО ПОЭТОМУ ЕСЛИ У ВАС 1 БИБЛИОТЕКА ЗАКОМЕНТИРУЙТЕ ЭТУ СТРОКУ
        .pipe(concat('libs.min.js')) // Собираем их в кучу в новом файле libs.min.js
        .pipe(plumber()) // Проверяем на ошибки
        .pipe(uglify()) // Сжимаем JS файл
        // .pipe(rename("libs.min.js")) // ЕСЛИ ФАЙЛОВ НЕСКОЛЬКО ОТКЛЮЧАЕМ ЭТУ СТРОКУ
        .pipe(gulp.dest('app/js')); // Выгружаем в папку app/js
});

// Экспорт скриптов JS в продакшн
gulp.task('exportScripts', function () {
    return gulp.src(['dev/js/**/*'])
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(gulp.dest('app/js'))
});

// Экспорт BOWER скриптов в папку dev/libs
gulp.task('bowerExport', function () {
    return gulp.src(mainBowerFiles({
        overrides: {
            select2: {
                main: [
                    'dist/css/select2.css',
                    'dist/js/select2.js'
                ]
            }
        }
    }))
        .pipe(gulp.dest('dev/libs'))
});


// Перенос шрифтов в продакшн
gulp.task('fonts', function () {
    return gulp.src(['dev/fonts/**/*'])
        .pipe(gulp.dest('app/fonts'))
});

// Сжатие и перенос изображений в продакшн
gulp.task('img', function () {
    return gulp.src('dev/img/**/*') // Берем все изображения из app
        .pipe(cache(imagemin({  // Сжимаем их с наилучшими настройками с учетом кеширования
            interlaced: true,
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        })))
        .pipe(gulp.dest('app/img')); // Выгружаем на продакшен
});
