'use strict';

$(function () {
    getData();
    removeItem();
});

var itemsId = [];
var currentId = void 0;

//Получение данных
function getData() {
    $.getJSON('https://rawgit.com/Varinetz/e6cbadec972e76a340c41a65fcc2a6b3/raw/90191826a3bac2ff0761040ed1d95c59f14eaf26/frontend_test_table.json', function (data) {
        $.each(data, function (key, el) {
            itemsId.push(el.id);
            addItem(key, el);
            getUniqId();
        });
    });
}

//Получение уникального id
function getUniqId() {
    currentId = 0;
    for (var i = 0; i <= itemsId.length; i++) {
        if (currentId == itemsId[i]) {
            currentId++;
            i = 0;
        }
    }
}

//Добавление элемента в таблицу
function addItem(key, el) {
    itemsId.push(currentId);
    var id = el.id;
    var title = el.title;
    var description = el.description;
    var hiddenClass = void 0;
    if (description.length == 0) {
        hiddenClass = 'char__block_empty';
    } else {
        hiddenClass = '';
    }
    var year = el.year;
    var color = void 0;
    switch (el.color) {
        case 'white':
            {
                color = 'color_white';
            }
            break;
        case 'black':
            {
                color = 'color_black';
            }
            break;
        case 'grey':
            {
                color = 'color_grey';
            }
            break;
        case 'red':
            {
                color = 'color_red';
            }
            break;
        case 'green':
            {
                color = 'color_green';
            }
            break;
        default:
            {
                color = '';
            }

    }
    var status = void 0;
    switch (el.status) {
        case 'in_stock':
            {
                status = 'В наличии';
            }
            break;
        case 'pednding':
            {
                status = 'Ожидается';
            }
            break;
        case 'out_of_stock':
            {
                status = 'Нет в наличии';
            }
            break;
        default:
            {
                status = 'Не известно';
            }
    }
    var price = priceFormat(el.price) + ' руб.';

    $('.cars__table').prepend('<div class="row char" id="id-' + id + '">' + '<div class="col-3 col-md-2 char__block char__block_title">' + '<span class="char__name char__text">' + title + '</span>' + '</div> <div class="col-1 hidden-md-up char__block"> ' + '<div class="char__color color ' + color + '"></div>' + ' </div>' + '<div class="col-2 col-md-1 push-md-2 char__block char__block_price pl-0">' + '<span class="char__price char__text">' + price + '</span>' + '</div>' + '<div class="col-6 char__block hidden-md-up ' + hiddenClass + '">' + '<span class="char__descr char__text char__text_light">' + description + '</span>' + '</div> <div class="col-1 col-md-1 pull-md-1 char__block char__block_year pr-md-0">' + '<span class="char__year char__text">' + year + '</span>' + '<div class="char__color color ' + color + ' hidden-sm-down">' + '</div>' + '</div>' + '<div class="col-3 col-md-1 pull-md-1 char__block char__block_status">' + '<span class="char__status char__text">' + status + '</span>' + '</div> <div class="col-2 col-md-1 char__block">' + '<button class="char__remove button button_small">Удалить</button>' + '</div> <div class="col-6 char__block hidden-sm-down ' + hiddenClass + '">' + '<span class="char__descr char__text char__text_light">' + description + '</span>' + '</div>' + '</div>');
}

//Удаление элемента
function removeItem() {
    $('.cars__table').on('click', '.char__remove', function (el) {
        var item = el.target.closest('.char');
        item.remove();
    });
}

//Преобразование данных формы в JSON - объект
function formToObject(formArray) {
    //serialize data function
    var object = {};
    for (var i = 0; i < formArray.length; i++) {
        object[formArray[i]['name']] = formArray[i]['value'];
    }
    return object;
}

//Получение нового элемента
$('.form').submit(function (e) {
    e.preventDefault();
    getUniqId();
    var formData = formToObject($(this).serializeArray());
    var newItem = {
        "id": currentId,
        "title": formData.name,
        "description": formData.description,
        "year": formData.year,
        "color": formData.color,
        "status": formData.status,
        "price": formData.price
    };
    addItem(null, newItem);
});